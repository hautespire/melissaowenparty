'use strict';

module.exports = function(grunt) {
  grunt.initConfig({

    properties: {
      buildPath: {
        dev: 'build/dev',
        live: 'build/live'
      },
    },

    eslint: {
      target: ['js/**/*.js', 'siteData/**/*.js']
    },

    /*
    connect: {
      server: {
        options: {
          livereload: 8080,
          port: 8080,
          base: 'build/dev'
        }
      }
    },
    */

    clean: {
      dev: '<%= properties.buildPath.dev %>/**/*',
      live: '<%= properties.buildPath.live %>/**/*'
    },

    copy: {
      dev: {
        files: [
          {'<%= properties.buildPath.dev %>/css/vendor/bootstrap.css':
          'bower_components/bootstrap/dist/css/bootstrap.css'},

          {'<%= properties.buildPath.dev %>/js/vendor/angular.js':
          'bower_components/angular/angular.js'},

          {'<%= properties.buildPath.dev %>/js/vendor/ui-bootstrap-tpls.js':
          'bower_components/angular-bootstrap/ui-bootstrap-tpls.js'},

          {'<%= properties.buildPath.dev %>/js/vendor/angular-ui-router.js':
          'bower_components/angular-ui-router/release/angular-ui-router.js'},

          {'<%= properties.buildPath.dev %>/index.html': 'index.html'},

          {'<%= properties.buildPath.dev %>/': 'sitedata/**/*'},

          {
            cwd: 'css/',
            src: ['**/*', '!vendor/**'],
            dest: '<%= properties.buildPath.dev %>/css',
            expand: true
          },
          {
            cwd: 'js/',
            src: '**/*',
            dest: '<%= properties.buildPath.dev %>/js',
            expand: true
          },
          {
            cwd: 'partials/',
            src: '**/*',
            dest: '<%= properties.buildPath.dev %>/partials',
            expand: true
          }
        ]
      },
      live: {
        files: [
          {'<%= properties.buildPath.live %>/css/vendor/bootstrap.css':
           'bower_components/bootstrap/dist/css/bootstrap.min.css'},

          {'<%= properties.buildPath.live %>/js/vendor/angular.js':
          'bower_components/angular/angular.min.js'},

          {'<%= properties.buildPath.live %>/js/vendor/ui-bootstrap-tpls.js':
          'bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js'},

          {'<%= properties.buildPath.live %>/js/vendor/angular-ui-router.js':
          'bower_components/angular-ui-router/release/angular-ui-router.min.js'},

          {'<%= properties.buildPath.live %>/index.html': 'index.html'},

          {'<%= properties.buildPath.live %>/': 'sitedata/**/*'},

          {
            cwd: 'css/',
            src: ['**/*', '!vendor/**'],
            dest: '<%= properties.buildPath.live %>/css',
            expand: true
          },
          {
            cwd: 'js/',
            src: '**/*',
            dest: '<%= properties.buildPath.live %>/js',
            expand: true
          },
          {
            cwd: 'partials/',
            src: '**/*',
            dest: '<%= properties.buildPath.live %>/partials',
            expand: true
          }
        ]
      }
    },

    watch: {
      dev: {
        files: [
          '**/*',
          '!bower_components/**',
          '!node_modules/**',
          '!build/**'
        ],
        tasks: ['clean:dev', 'eslint', 'htmllint', 'copy:dev'],
      },
      options: {
        livereload: true
      }
    },

    htmllint: {
      options: {
        'attr-name-style': 'dash', // 'dash' means allow lowercase and dashes
        'class-style': 'dash',
        'tag-bans': ['marquee'],
        'tag-close': true,
        'tag-name-lowercase': true,
        'tag-name-match': true,
        'spec-char-escape': true,
        'line-end-style': 'crlf'
      },
      src: [
        '**/*.html',
        '!bower_components/**',
        '!node_modules/**',
        '!build/**'
      ]
    }
  });

  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-connect');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-eslint');
  grunt.loadNpmTasks('grunt-htmllint');

  grunt.registerTask('default',
  ['clean:dev', 'eslint', 'htmllint', 'copy:dev']);

  grunt.registerTask('dev', ['default']);
  grunt.registerTask('live', ['clean:live', 'eslint', 'htmllint', 'copy:live']);

  grunt.registerTask('clean-all', ['clean:dev', 'clean:live']);
};