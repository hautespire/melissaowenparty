// Hotel directive for displaying hotel info
'use strict';
angular.module('mopApp').directive('hotelInfo', function() {
  var hotelDirective = {};

  hotelDirective.restrict = 'E';
  hotelDirective.scope = {
    hotel: '='
  };
  hotelDirective.templateUrl = 'js/directives/partials/hotel.html';

  return hotelDirective;
});