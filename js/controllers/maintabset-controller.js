//// main-tabset
// Controller that controls the tabset and tabs
'use strict';
/* eslint-disable no-invalid-this */

angular.module('mopApp').controller('MainTabsetController',
function($log) {
  var vm = this;

  vm.tabs = [
    /*
    {
      'title': 'Home',
      'templates': [
        '/partials/tab_faq.html'
      ],
      'active': true
    },
    */
    {
      'title': 'Ceremony & Reception',
      'templates': [
        '/partials/tab_ceremonyreception.html'
      ],
      'active': false
    },
    {
      'title': 'Hotel Info',
      'templates': [
        '/partials/tab_hotels.html'
      ],
      'active': false
    },
    {
      'title': 'Registry',
      'templates': [
        '/partials/tab_registry.html'
      ],
      'active': false,
      'icon': 'fa-star-o'
    }
  ];
});