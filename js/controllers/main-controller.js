//// main-controller
// Controller that controls the main body of the site.
'use strict';
/* eslint-disable no-invalid-this */

angular.module('mopApp').controller('MainController',
function($log, HotelsService, SlidesService) {
  var vm = this;

  vm.hotels = [];

  vm.carousel = {
    show: true,
    landscape: {
      active: 0
    },
    slideInterval: 10000, //ms
    slides: []
  };

  var setHotels = function() {
    HotelsService.getAll().then(
      function(response) {
        vm.hotels = response.data.hotels || [];
        $log.info('GET Hotels: success');
      },
      function(err) {
        $log.error('GET Hotels: error.\nMessage:' + err);
      }
    );
  };

  var setSlides = function() {
    SlidesService.getAll().then(
      function(response) {
        vm.carousel.slides = response.data || [];
      },
      function(err) {
        $log.error('GET Slides: error.\nMessage:' + err);
      }
    );
  };

  var init = function() {
    setHotels();
    setSlides();
  };

  init();
});