////get-json
// Retrieve data from a local JSON file.
'use strict';
angular.module('mopApp').factory('HotelsService',
function($http) {
  var service = {};

  service.getAll = function() {
    return $http.get('/sitedata/hotel-info.json');
  };

  return service;
});