////get-slides
// Retrieve slides from JSON file.
'use strict';
angular.module('mopApp').factory('SlidesService',
function($http) {
  var service = {};

  service.getAll = function() {
    return $http.get('/sitedata/slides-info.json');
  };

  return service;
});